# RIPE64: a 64bit port of the Runtime Intrusion Prevention Evaluator
This repository presents a 64-bit port of the [RIPE benchmark](https://github.com/johnwilander/RIPE).
RIPE was originally developed by John Wilander and Nick Nikiforakis and
presented at the 2011 Annual Computer Security Applications Conference (ACSAC) in Orlando, Florida.

This port was developed by Hubert ROSIER for his master thesis in the National University of Singapore.
The project was supervised by Professor Roland YAP and co-supervisor senior research fellow Gregory James DUCK
of the School of Computing of the National University of Singapore.

The 850 buffer overflow attacks implemented in the original version of RIPE has been re-implemented to work as a 64bit software.  
Few more attacks have been added also and now RIPE64 can run around than 2050 buffer overflow attack forms if
we consider each shellcode as different form else it has around 950 different attack forms.

## How to build and run
#### Build
To build the benchmark just run the `make` command.
It will create two executable file in the `build/` folder, one compiled by
`gcc` and the other by `clang`.
It will be compiled without stack protector (`-fno-stack-protector`) and with executable 
stack (`-z execstack`).  
ASLR can be disabled temporary with:  
```bash
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

#### Individual test
To run a specific attack, you need to specify all the dimensions like this:
```bash
 ./build/[gcc|clang]_attack_gen -l location -c code_ptr -i inject_param -t [direct|indirect] -f func_abused [-d t|]
```
where:  
  - __location__ can be "stack", "heap", "bss" or "data"
  - __code\_ptr__ can be "ret", "baseptr", "funcptrstackvar", "funcptrstackparam",
"funcptrheap", "funcptrbss", "funcptrdata", "structfuncptrstack", "structfuncptrheap",
"structfuncptrbss", "structfuncptrdata", "longjmpstackvar", "longjmpstackparam",
"longjmpheap", "longjmpbss" or "longjmpdata"
  - __inject\_params__ can be "nonop","simplenop", "simplenopequival", "r2libc" or "rop"
  - __func\_abused__ can be "memcpy", "strcpy", "strncpy", "sprintf", "snprintf",
    "strcat", "strncat", "sscanf", "fscanf" or "homebrew"

The attacks is successful is a shell has been spawned.  

#### Full benchmark
You can run all the possible attack forms by running the script `ripe_tester.py`:  
```bash
 ./ripe_tester.py [direct|indirect|both] n (gcc|clang|)
```

It accepts 2 or 3 parameters, the first one to launch direct attacks, indirect or both;
the second is the number of times each attack should be launched and the last one to use the gcc or clang executables or both.

Successful attacks are logged as "OK", the ones that failed are "FAILED", the ones that didn't succeed each round
are marked as SOME.  
The attacks logged as "NOT POSSIBLE" are the ones that are considered impossible such as overflowing a function pointer in the bss segment from the stack.

## Issues
### Variable placement
Some attacks are failing because in some cases, variables are not placed as wanted.  
For instance, gcc places a function pointer passed as parameter at the top of the stack below the
stack buffers making a direct buffer overflow attack impossible with these buffers.  
It is still possible to overflow a buffer that is below for instance one of the next stack frame or to do a underflow of the current stack frame.
These solutions have not been implemented.  

An other example is that gcc seems to place bss buffer at the top of the segment,
 so above the bss targets making direct buffer overflow attack not possible.
Underflow attacks can be launched in this case (not implemented yet).

There is also an issue with some indirect attacks on jump buffers because it is located
between the buffer and the target generic pointer and then all its values are overflowed and raise errors.
It is the case for indirect attack against the data jump buffer from the data location when compiled with clang and the indirect attack from bss segment on the bss\_jmp\_buffer with gcc.  
A solution could be to include the original values in the buffer.  

### Zeros in addresses
Most of the function abused are only copying the payload until there is a terminating character.
In some attack form, there is always zeros in the payload especially the attacks that are doing
return oriented programming because almost all 64bit addresses have zeros in them.  

The following attacks are failing for this reason:  
  - direct attack on return addr with r2libc and rop with null-sensible functions
  - attack on old base ptr with null-sensible functions

## Note
### Ropper
[Ropper](https://github.com/sashs/Ropper) can find gadgets to build rop chains for different architectures.
It was used before the wanted gadgets were hardcoded in functions.

### Metasploit
The one byte NOP equivalent sled has been generated using the metasploit framework 
with the command:  
```
generate 40 -s rsp -t c
```
the `-s rsp` tells that we don't want to change the RSP register (I got errors without).  

[how to install](https://www.darkoperator.com/installing-metasploit-in-ubunt/)
